# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.
require 'byebug'

class TowersOfHanoi
  attr_reader :towers
  def initialize(tower1 = 3, tower2 = 0, tower3 = 0)
    #debugger
    @towers = Array.new(3) { Array.new(0) }
    @towers[0] = [*1..tower1].reverse
  end

  def play
    until won?
      render
      from_pile = get_input('What pile woud you like to move from?') - 1
      to_pile = get_input('What pile woud you like to move to?', from_pile + 1) - 1
      valid_move?(from_pile, to_pile) ? move(from_pile, to_pile) : (puts 'invalid move')
    end
    render
    puts 'Yay, you won!'
  end

  def get_input(message, avoid = 0)
    pile = nil
    until [*1..3].include?(pile) && pile != avoid
      puts 'bad input, try again' if pile
      puts message
      pile = gets.chomp.to_i
    end
    pile
  end

  def render
    puts " T1  T2  T3"
    3.times do |pass|
      level = 3 - pass
      @towers.each do |tower|
        disc_size = tower[level - 1] || 0
        print '_' * disc_size  if tower.length >= level
        print ' ' * (4 - disc_size)
      end
      puts "\n"
    end
  end

  def won?
    @towers[0].empty? && (@towers[1].empty? || @towers[2].empty?)
  end

  def valid_move?(from, to)
    f_tower = @towers[from]
    t_tower = @towers[to]
    !f_tower.empty? && (t_tower.empty? || t_tower[-1] > f_tower[-1])
  end

  def move(from, to)
    @towers[to] << @towers[from].pop
  end
end

TowersOfHanoi.new.play
